import common.Demo;
import lr7.LR7Demo;

public class Main {
    static Demo demo = new LR7Demo();
    public static void main(String[] args) {
        demo.run();
    }
}
