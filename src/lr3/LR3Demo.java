package lr3;

import common.Demo;
import lr3.algorithm.AES;

//import lr3.algorithm.AES;

public class LR3Demo implements Demo {
    protected final String ENCODING_MESSAGE = "Encoding/decoding demonstration:";
    protected final String TEST_TEXT_1 = "ABCDEFGHIJKLMNOPQRSTUVWXYZ abcdefghijklmnopqrstuvwxyz";
    protected final String TEST_TEXT_2 = "As a fragment of soul, a Horcrux seemed to retain the identity of its " +
            "creator at the time of its creation. Voldemort, for instance, created a Horcrux during what was " +
            "presumably his fifth year at Hogwarts. As such, the fragment of soul contained within the Horcrux " +
            "took on the appearance and mannerisms of Voldemort as he had been when he was 16 years old.";
    protected final String cryptKey = "aPb4x9q0H4W8rPs7";

    public void run() {
        AES aes = new AES();
        aes.setKey(cryptKey);
        String encryptedData = aes.encrypt(TEST_TEXT_1);
        String decryptedData = aes.decrypt(encryptedData);
        System.out.println(ENCODING_MESSAGE);
        System.out.println(TEST_TEXT_1);
        System.out.println(encryptedData);
        System.out.println(decryptedData);

        encryptedData = aes.encrypt(TEST_TEXT_2);
        decryptedData = aes.decrypt(encryptedData);
        System.out.println("\n\n" + TEST_TEXT_2);
        System.out.println(encryptedData);
        System.out.println(decryptedData);
    }
}
