package lr3.algorithm;

import lr3.algorithm.utils.Mode;
import common.Util;

/** AES - implementation of the AES block cipher.
 *  Illustrative code for the AES block cipher.
 *  Handles a single block encryption or decryption.
 *  AES is a block cipher with a key length of 16/24/32 bytes
 *  and a block length 16 bytes.
 *
 *  @see <a href="http://www.unsw.adfa.edu.au/~lpb/">Lawrie Brown</a>
 *  @see <a href="http://csrc.nist.gov/encryption/aes/">AES home page</a>
 *  @see <a href="http://csrc.nist.gov/publications/fips/fips197/fips-197.pdf">FIPS-197 Standard</a>
 *  @see <a href="http://www.esat.kuleuven.ac.be/~rijmen/rijndael/">Rijndael Home Page</a>
 *  @see <a href="http://www.esat.kuleuven.ac.be/~rijmen/rijndael/rijndael.zip">Rijndael example Java code</a>
 */
public class AES {
    private int numRounds;
    private byte[][] keysEncryption;
    private byte[][] keysDecryption;

    public static final int BLOCK_SIZE = 16;                     // AES uses 128-bit (16 byte) key
    public static final int COL_SIZE = 4;                        // depth of each column in AES state variable
    public static final int NUM_COLS = BLOCK_SIZE / COL_SIZE;    // number of columns in AES state variable
    public static final int ROOT = 0x11B;                        // generator polynomial used in GF(2^8)

    static final int[] row_shift = {0, 1, 2, 3};
    static final int[] GF28D = new int[256]; // GF28D table for field GF(2^m).
    static final int[] GF28E = new int[256]; // GF28E table for field GF(2^m).

    /** Initialization of the GF28E and GF28D tables.
     *  Used to implement multiplication in GF(2^8).
     */
    static {
        int i, j;
        // produce GF28E and GF28D tables, needed for multiplying in the field GF(2^8)
        GF28D[0] = 1;
        for (i = 1; i < 256; i++) {
            j = (GF28D[i - 1] << 1) ^ GF28D[i-1];
            if ((j & 0x100) != 0) j ^= ROOT;
            GF28D[i] = j;
        }
        for (i = 1; i < 255; i++) {
            GF28E[GF28D[i]] = i;
        }
    }

    public String encrypt(String data) {
        return splitSourceString(data, Mode.ENCRYPTION);
    }
    public String decrypt(String data) {
        return splitSourceString(data, Mode.DECRYPTION);
    }

    private String splitSourceString(String data, Mode mode)  {
        AES aes = this;
        if (data.length() % 16 > 0) {
            int rest = 16 - (data.length() % 16);
            for(int i = 0; i < rest; i++) {
                data += " ";
            }
        }
        int nParts = data.length() / 16;
        byte[] res = new byte[data.length()];
        String partStr;
        byte[] partByte;
        for(int p = 0; p < nParts; p++) {
            partStr = data.substring(p * 16, p * 16 + 16);
            partByte = Util.stringToByteArray(partStr);
            partByte = (mode == Mode.ENCRYPTION) ?
                    aes.encrypt(partByte) :
                    aes.decrypt(partByte);
            for(int b = 0; b < 16; b++) {
                res[p * 16 + b] = partByte[b];
            }
        }
        return Util.byteArrayToString(res);
    }

    private byte[] encrypt(byte[] source) {
        byte [] state;

        // Do initial AddRoundKey(state).
        state = addRoundKey(source, keysEncryption[0]);

        // for each round except last, apply round transforms
        for (int r = 1; r < numRounds; r++) {
            state = subBytes(state, Mode.ENCRYPTION);
            state = shiftRows(state, Mode.ENCRYPTION);
            state = mixColumns(state, Mode.ENCRYPTION);
            state = addRoundKey(state, keysEncryption[r]);
        }

        // Last round - without mix columns.
        state = subBytes(state, Mode.ENCRYPTION);
        state = shiftRows(state, Mode.ENCRYPTION);
        state = addRoundKey(state, keysEncryption[numRounds]);

        return state;
    }

    private byte[] decrypt(byte[] cipher) {
        byte [] state;

        if (cipher == null)
            throw new IllegalArgumentException("Empty ciphertext");
        if (cipher.length != BLOCK_SIZE)
            throw new IllegalArgumentException("Incorrect ciphertext length");

        // Do initial AddRoundKey(state).
        state = addRoundKey(cipher, keysDecryption[0]);

        // for each round except last, apply round transforms
        for (int r = 1; r < numRounds; r++) {
            state = shiftRows(state, Mode.DECRYPTION);
            state = subBytes(state, Mode.DECRYPTION);
            state = addRoundKey(state, keysDecryption[r]);
            state = mixColumns(state, Mode.DECRYPTION);
        }

        // last round - without mix lolumns
        state = shiftRows(state, Mode.DECRYPTION);
        state = subBytes(state, Mode.DECRYPTION);
        state = addRoundKey(state, keysDecryption[numRounds]);

        return state;
    }

    private byte[] subBytes(byte[] source, Mode mode) {
        byte[] result = new byte[source.length];
        SBox sBox = SBox.getInstance(mode);

        for (int i = 0; i < BLOCK_SIZE; i++) {
            result[i] = sBox.getByte(source[i] & 0xFF);
        }
        return result;
    }

    private byte[] shiftRows(byte[] source, Mode mode) {
        byte[] result = new byte[source.length];
        int row, k;

        for (int i = 0; i < BLOCK_SIZE; i++) {
            row = i % COL_SIZE;
            k = (mode == Mode.ENCRYPTION) ? (i + (row_shift[row] * COL_SIZE)) % BLOCK_SIZE :
                                            (i + BLOCK_SIZE - (row_shift[row] * COL_SIZE)) % BLOCK_SIZE;
            result[i] = source[k];
        }
        return result;
    }

    private byte[] mixColumns(byte[] source, Mode mode) {
        byte[] result = new byte[source.length];
        int i;

        for (int col = 0; col < NUM_COLS; col++) {
            i = col * COL_SIZE;
            if (mode == Mode.ENCRYPTION) {
                result[i] = (byte)(mul(2, source[i]) ^ mul(3, source[i + 1]) ^ source[i + 2] ^ source[i + 3]);
                result[i + 1] = (byte)(source[i] ^ mul(2, source[i + 1]) ^ mul(3, source[i + 2]) ^ source[i + 3]);
                result[i + 2] = (byte)(source[i] ^ source[i + 1] ^ mul(2, source[i + 2]) ^ mul(3, source[i + 3]));
                result[i + 3] = (byte)(mul(3, source[i]) ^ source[i + 1] ^ source[i + 2] ^ mul(2, source[i + 3]));
            } else {
                result[i] = (byte)(mul(0x0e, source[i]) ^ mul(0x0b, source[i + 1]) ^ mul(0x0d, source[i + 2]) ^ mul(0x09, source[i + 3]));
                result[i + 1] = (byte)(mul(0x09, source[i]) ^ mul(0x0e, source[i + 1]) ^ mul(0x0b, source[i + 2]) ^ mul(0x0d, source[i + 3]));
                result[i + 2] = (byte)(mul(0x0d, source[i]) ^ mul(0x09, source[i + 1]) ^ mul(0x0e, source[i + 2]) ^ mul(0x0b, source[i + 3]));
                result[i + 3] = (byte)(mul(0x0b, source[i]) ^ mul(0x0d, source[i + 1]) ^ mul(0x09, source[i + 2]) ^ mul(0x0e, source[i + 3]));
            }
        }
        return result;
    }

    private byte[] addRoundKey(byte[] source, byte[] keysCurrentRound) {
        byte[] result = new byte[source.length];

        for (int i = 0; i < BLOCK_SIZE; i++) {
            result[i] = (byte) (source[i] ^ keysCurrentRound[i]);
        }

        return result;
    }

    public static int getRounds (int keySize) {
        switch (keySize) {
            case 16:
                return 10;
            case 24:
                return 12;
            default:
                return 14;
        }
    }

    // Multiply two elements of GF(2^8).
    static final int mul (int a, int b) {
        return (a != 0 && b != 0) ? GF28D[(GF28E[a & 0xFF] + GF28E[b & 0xFF]) % 255] : 0;
    }

    public void setKey(String key) {
        ExpandKey expandKey = new ExpandKey();
        expandKey.setKey(key);
        numRounds = expandKey.getNumRounds();
        keysEncryption = expandKey.getKeysEncryption();
        keysDecryption = expandKey.getKeysDecryption();
    }
}