package lr3.algorithm.utils;

public enum Mode {
    ENCRYPTION,
    DECRYPTION
}
