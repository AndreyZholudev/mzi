package lr3.algorithm;

import lr3.algorithm.utils.Mode;
import common.Util;

import static lr3.algorithm.AES.BLOCK_SIZE;
import static lr3.algorithm.AES.getRounds;

public class ExpandKey {
    private static final byte[] Rcon = {
            0,
            1, 2, 4, 8, 16, 32,
            64, -128, 27, 54, 108, -40,
            -85, 77, -102, 47, 94, -68,
            99, -58, -105, 53, 106, -44,
            -77, 125, -6, -17, -59, -111 };

    private int numRounds;
    private byte[][] keysEncryption;
    private byte[][] keysDecryption;

    public void setKey(byte[] key) {
        final int wordsAmount = BLOCK_SIZE / 4;
        final int keyLength = key.length;
        final int columnsAmount = keyLength / 4;

        int i, j, r;

        if (key == null)
            throw new IllegalArgumentException("Empty key");
        if (!(key.length == 16 || key.length == 24 || key.length == 32))
            throw new IllegalArgumentException("Incorrect key length");

        numRounds = getRounds(keyLength);
        final int ROUND_KEY_COUNT = (numRounds + 1) * wordsAmount;

        // Each array holds one row of the state.
        byte[] w0 = new byte[ROUND_KEY_COUNT];
        byte[] w1 = new byte[ROUND_KEY_COUNT];
        byte[] w2 = new byte[ROUND_KEY_COUNT];
        byte[] w3 = new byte[ROUND_KEY_COUNT];

        keysEncryption = new byte[numRounds + 1][BLOCK_SIZE];
        keysDecryption = new byte[numRounds + 1][BLOCK_SIZE];

        // Copy key into start of session array.
        for (i = 0, j = 0; i < columnsAmount; i++) {
            w0[i] = key[j++];
            w1[i] = key[j++];
            w2[i] = key[j++];
            w3[i] = key[j++];
        }

        // Key expansion implementation.
        byte t0, t1, t2, t3, old0;
        SBox sBox = SBox.getInstance(Mode.ENCRYPTION);
        for (i = columnsAmount; i < ROUND_KEY_COUNT; i++) {
            t0 = w0[i - 1];
            t1 = w1[i - 1];
            t2 = w2[i - 1];
            t3 = w3[i - 1];
            if (i % columnsAmount == 0) {
                old0 = t0;
                t0 = (byte)(sBox.getByte(t1 & 0xFF) ^ Rcon[i/columnsAmount]);
                t1 = (sBox.getByte(t2 & 0xFF));
                t2 = (sBox.getByte(t3 & 0xFF));
                t3 = (sBox.getByte(old0 & 0xFF));
            }
            else if ((columnsAmount > 6) && (i % columnsAmount == 4)) {
                t0 = sBox.getByte(t0 & 0xFF);
                t1 = sBox.getByte(t1 & 0xFF);
                t2 = sBox.getByte(t2 & 0xFF);
                t3 = sBox.getByte(t3 & 0xFF);
            }
            w0[i] = (byte)(w0[i - columnsAmount] ^ t0);
            w1[i] = (byte)(w1[i - columnsAmount] ^ t1);
            w2[i] = (byte)(w2[i - columnsAmount] ^ t2);
            w3[i] = (byte)(w3[i - columnsAmount] ^ t3);
        }

        // Copy values into keys arrays.
        for (r = 0, i = 0; r < numRounds + 1; r++) {
            for (j = 0; j < wordsAmount; j++) {
                keysEncryption[r][4 * j] = w0[i];
                keysEncryption[r][4 * j + 1] = w1[i];
                keysEncryption[r][4 * j + 2] = w2[i];
                keysEncryption[r][4 * j + 3] = w3[i];
                keysDecryption[numRounds - r][4 * j] = w0[i];
                keysDecryption[numRounds - r][4 * j + 1] = w1[i];
                keysDecryption[numRounds - r][4 * j + 2] = w2[i];
                keysDecryption[numRounds - r][4 * j + 3] = w3[i];
                i++;
            }
        }
    }

    public void setKey(String key) {
        setKey(Util.stringToByteArray(key));
    }

    public int getNumRounds() {
        return numRounds;
    }

    public byte[][] getKeysEncryption() {
        return keysEncryption;
    }

    public byte[][] getKeysDecryption() {
        return keysDecryption;
    }
}
