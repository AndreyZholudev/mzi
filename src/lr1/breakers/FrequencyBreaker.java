package lr1.breakers;

import lr1.algorithms.Cesar;
import lr1.utils.Constants;

import java.text.DecimalFormat;

public class FrequencyBreaker {
    protected final double[] ENGLISH_FREQUENCY = {8.17, 1.49, 2.78, 4.25, 12.7, 2.23, 2.02, 6.09, 6.97, 0.15, // A-J
                                                  0.77, 4.03, 2.41, 6.75, 7.51, 1.93, 0.1, 5.99, 6.33, 9.06,  // K-T
                                                  2.76, 0.98, 2.36, 0.15, 1.97, 0.05}; // U-Z
    protected final String FREQUENCES_MESSAGE = "\n1st line - statistical frequences of english letters in general, " +
            "2nd - in the input text.";
    protected final String CRITERIA_MESSAGE = "\n\nCriteria, computed with each offset:";
    protected final String RESULT_OFFSET_MESSAGE = "\nResult offset:";

    DecimalFormat df = new DecimalFormat("##.00");
    protected double[] result = new double[Constants.ENGLISH_LENGTH];
    protected int[] counter = new int[Constants.ENGLISH_LENGTH];
    protected String source;
    protected int n;
    protected int resultOffset;

    public FrequencyBreaker(String source) {
        this.source = source;
        countSymbols();
        analyze();
    }

    protected void countSymbols() {
        for(byte b : source.getBytes()) {
            if (Constants.a_OFFSET <= b && b <= Constants.z_OFFSET ) {
                counter[b - Constants.a_OFFSET]++;
            } else
            if (Constants.A_OFFSET <= b && b <= Constants.Z_OFFSET ) {
                counter[b - Constants.A_OFFSET]++;
            }
        }

        for(int v : counter) {
            n += v;
        }

        System.out.println(FREQUENCES_MESSAGE);

        for(int i = 0; i < Constants.ENGLISH_LENGTH; i++) {
            System.out.format("%6s", df.format(ENGLISH_FREQUENCY[i]));
        }
        System.out.println();
        for(int i = 0; i < Constants.ENGLISH_LENGTH; i++) {
            result[i] = 100 * counter[i] / (double)n;
            System.out.format("%6s", df.format(result[i]));
        }
    }

    protected void analyze() {
        double min = Double.MAX_VALUE;
        double sum = 0;

        System.out.println(CRITERIA_MESSAGE);
        for (int i = 0; i < Constants.ENGLISH_LENGTH; i++) {
            System.out.format("%8s", i);
        }
        System.out.println();

        for (int offset = Constants.ENGLISH_LENGTH; offset > -1; offset--) {
            for (int j = 0; j < Constants.ENGLISH_LENGTH; j++) {
                sum += Math.abs(ENGLISH_FREQUENCY[j] -
                        result[(j - offset + Constants.ENGLISH_LENGTH) % Constants.ENGLISH_LENGTH]);
            }
            if (sum < min) {
                min = sum;
                resultOffset = offset;
            }
            System.out.format("%8s", df.format(sum));
            sum = 0;
        }
        resultOffset = Constants.ENGLISH_LENGTH - resultOffset;
        System.out.println(RESULT_OFFSET_MESSAGE);
        System.out.println(resultOffset);
    }

    public String decrypt() {
        Cesar cesar = new Cesar(resultOffset);
        return cesar.decrypt(source);
    }
}
