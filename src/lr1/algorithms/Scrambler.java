package lr1.algorithms;

import common.BitSetUtil;

import java.util.BitSet;

public class Scrambler {
    BitSet key = BitSet.valueOf(new byte[] {99});
    public String encrypt(String source) {
        StringBuilder result = new StringBuilder(source.length());
        BitSet mid;
        for(byte b : source.getBytes()) {
            mid = BitSet.valueOf(new byte[] {b});
            mid.xor(key);
            result.append((char) BitSetUtil.bitSetToByte(mid));
        }
        return result.toString();
    }
}
