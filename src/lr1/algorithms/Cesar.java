package lr1.algorithms;

import lr1.utils.Constants;

public class Cesar {
    private int offset;

    public Cesar(int offset) {
        this.offset = offset;
    }

    public String encrypt(String source) {
        StringBuilder result = new StringBuilder(source.length());
        for (byte b : source.getBytes()) {
            if (Constants.a_OFFSET <= b && b <= Constants.z_OFFSET) {
                result.append((char)((b + offset)
                        % Constants.a_OFFSET % Constants.ENGLISH_LENGTH + Constants.a_OFFSET));
            } else
            if(Constants.A_OFFSET <= b && b <= Constants.Z_OFFSET) {
                result.append((char)((b + offset)
                        % Constants.A_OFFSET % Constants.ENGLISH_LENGTH + Constants.A_OFFSET));
            }
            else {
                result.append((char)b);
            }
        }
        return result.toString();
    }

    public String decrypt(String source) {
        StringBuilder result = new StringBuilder(source.length());
        for (byte b : source.getBytes()) {
            if (Constants.a_OFFSET <= b && b <= Constants.z_OFFSET) {
                result.append((char)(Constants.a_OFFSET
                        + (b - offset) % Constants.a_OFFSET % (Constants.a_OFFSET - Constants.ENGLISH_LENGTH)));
            } else
            if(Constants.A_OFFSET <= b && b <= Constants.Z_OFFSET) {
                result.append((char)(Constants.A_OFFSET
                        + (b - offset) % Constants.A_OFFSET % (Constants.A_OFFSET - Constants.ENGLISH_LENGTH)));
            }
            else {
                result.append((char)b);
            }
        }
        return result.toString();
    }
}
