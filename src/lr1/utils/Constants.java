package lr1.utils;

public class Constants {
    private Constants() {

    }

    public static final int A_OFFSET = 65;
    public static final int Z_OFFSET = 90;
    public static final int a_OFFSET = 97;
    public static final int z_OFFSET = 122;
    public static final int ENGLISH_LENGTH = 26;
}
