package lr1;

import common.Demo;
import lr1.algorithms.Cesar;
import lr1.algorithms.Scrambler;
import lr1.breakers.FrequencyBreaker;

public class LR1Demo implements Demo {
    protected final String ENCODING_MESSAGE = "Encoding/decoding demonstration:";
    protected final String DECODED_WITH_BREAKER = "\nMessage, decoded with frequency breaker:";
    protected final String TEST_TEXT_1 = "ABCDEFGHIJKLMNOPQRSTUVWXYZ abcdefghijklmnopqrstuvwxyz";
    protected final String TEST_TEXT_2 = "As a fragment of soul, a Horcrux seemed to retain the identity of its " +
            "creator at the time of its creation. Voldemort, for instance, created a Horcrux during what was " +
            "presumably his fifth year at Hogwarts. As such, the fragment of soul contained within the Horcrux " +
            "took on the appearance and mannerisms of Voldemort as he had been when he was 16 years old.";


    private Cesar cesar;
    @Override
    public void run() {
        cesar = new Cesar(3);
        System.out.println(ENCODING_MESSAGE);
        System.out.println(TEST_TEXT_1);
        String encrypted1 = cesar.encrypt(TEST_TEXT_1);
        System.out.println(encrypted1);
        System.out.println(cesar.decrypt(encrypted1));
        System.out.println(TEST_TEXT_2);
        String encrypted2 = cesar.encrypt(TEST_TEXT_2);
        System.out.println(encrypted2);
        System.out.println(cesar.decrypt(encrypted2));

        FrequencyBreaker fb = new FrequencyBreaker(cesar.encrypt(TEST_TEXT_2));
        System.out.println(DECODED_WITH_BREAKER);
        System.out.println(fb.decrypt());


        System.out.println("\n\n\nScrambler; encrypted text:");
        Scrambler scrambler = new Scrambler();
        String scramblerResult = scrambler.encrypt(TEST_TEXT_2);
        System.out.println(scramblerResult);

        System.out.println("Decrypted result:");
        System.out.println(scrambler.encrypt(scramblerResult));
    }
}
