package common;

import java.util.BitSet;

public class BitSetUtil {
    public static byte bitSetToByte(BitSet source) {
        byte result = 0;
        for (int i = 0; i < source.length(); i++) {
            if (source.get(i)) {
                result |= 1 << (i % 8);
            }
        }
        return result;
    }

    /**
     *
     * @param source BitSet instance.
     * @param from Position of bit, get value from (inclusive).
     * @param to Position of bit, get value to (exclusive).
     * @return Byte value of specified bit representation.
     */
     public static byte bitSetToByte(BitSet source, byte from, byte to) {
        byte result = 0;
        if (from < 0 || from > source.length() || to < 0 || to > source.length()) {
            return 0;
        }
        for (int i = from; i < to; i++) {
            if (source.get(i)) {
                result |= 1 << ((to - i - 1) % 8);
            }
        }
        return result;
    }

    public static byte[] bitSetToByteArray(BitSet source) {
        byte[] result = new byte[(source.length() - 1) / 8 + 1];
        for (int i = 0; i < (source.length() - 1) / 8 + 1; i++) {
            byte from = (byte)(i * 8);
            byte to = (byte)((i + 1) * 8);
            if (source.length() < to) {
                to = (byte)source.length();
            }
            result[i] = bitSetToByte(source, from, to);
        }
        return result;
    }

    public static String t;
    public static BitSet joinFixed(BitSet a, BitSet b, int size) {
        BitSet result = new BitSet(size * 2);
        for (int i = 0; i < size; i++) {
            result.set(i, a.get(i));
        }
        for (int i = 0; i < size; i++) {
            result.set(i + size, b.get(i));
        }
        return result;
    }

    public static String byteArrayToString(byte[] source) {
        try {
            return new String(source, "US-ASCII");
        } catch(Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
