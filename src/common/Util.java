package common;

import java.nio.ByteBuffer;

/**
 *   collection of utility routines for converting and displaying binary
 *   saved in byte/short/int arrays, and loaded/displayed using hex.
 *
 *   @author Lawrie Brown, Oct 2001
 */
public class Util {
    public static String byteArrayToString(byte[] source) {
        String result;
        StringBuffer sb = new StringBuffer();
        for(int i = 0; i < source.length; i++) {
            int n = (int)source[i];
            if (n < 0) {
                n += 256;
            }
            sb.append((char)n);
        }
        result = sb.toString();
        return result;
    }

    public static byte[] stringToByteArray(String source){
        byte[] result = new byte[source.length()];
        for(int i = 0; i < source.length(); i++){
            result[i] = (byte)source.charAt(i);
        }
        return result;
    }

    public static byte[] longToByteArray(Long l) {
        return ByteBuffer.allocate(Long.SIZE / Byte.SIZE).putLong(l).array();
    }

    public static Long byteArrayToLong(byte[] array) {
        long value = 0;
        for (int i = 0; i < array.length; i++)
        {
            value = (value << 8) + (array[i] & 0xff);
        }
        return value;
    }
}
