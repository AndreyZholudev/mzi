package lr2;

import common.Demo;
import lr2.algorithms.DES;

import java.util.BitSet;

public class LR2Demo implements Demo {
    protected final Long[] l = {4217864213897214213l, 2846529573527484637l, -5985238748127539563l};
    protected final String ENCODING_MESSAGE = "Encoding/decoding demonstration:";
    protected final String TEST_TEXT_1 = "ABCDEFGHIJKLMNOPQRSTUVWXYZ abcdefghijklmnopqrstuvwxyz";
    protected final String TEST_TEXT_2 = "As a fragment of soul, a Horcrux seemed to retain the identity of its " +
            "creator at the time of its creation. Voldemort, for instance, created a Horcrux during what was " +
            "presumably his fifth year at Hogwarts. As such, the fragment of soul contained within the Horcrux " +
            "took on the appearance and mannerisms of Voldemort as he had been when he was 16 years old.";

    public void run() {
        DES des = new DES(new BitSet[] {
                BitSet.valueOf(new long[] {l[0]}),
                BitSet.valueOf(new long[] {l[1]}),
                BitSet.valueOf(new long[] {l[2]})
        });
        String encryptedData = des.encrypt(TEST_TEXT_1);
        String decryptedData = des.decrypt(encryptedData);
        System.out.println(ENCODING_MESSAGE);
        System.out.println(TEST_TEXT_1);
        System.out.println(encryptedData);
        System.out.println(decryptedData);

        encryptedData = des.encrypt(TEST_TEXT_2);
        decryptedData = des.decrypt(encryptedData);
        System.out.println("\n\n" + TEST_TEXT_2);
        System.out.println(encryptedData);
        System.out.println(decryptedData);
    }
}
