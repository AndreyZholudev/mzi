package lr2.algorithms;

import common.BitSetUtil;
import lr2.algorithms.desalghorithms.Feistel;
import lr2.algorithms.desalghorithms.KeyGenerator;
import lr2.algorithms.desalghorithms.swaps.EndSwap;
import lr2.algorithms.desalghorithms.swaps.InitialSwap;

import java.util.Arrays;
import java.util.BitSet;
import java.util.Random;

public class DES {
    protected BitSet[] key;
    protected KeyGenerator[] keyGenerator;

    public DES() {
        init();
    }

    public DES(BitSet[] key) {
        if (key.length != 3) {
            throw new RuntimeException("Key array size must have length of 3 elements!");
        }
        this.key = key;
        init();
    }

    private void init() {
        Random random = new Random();
        key = new BitSet[] {
                BitSet.valueOf(new long[] {random.nextLong()}),
                BitSet.valueOf(new long[] {random.nextLong()}),
                BitSet.valueOf(new long[] {random.nextLong()})
        };
        keyGenerator = new KeyGenerator[] {
                new KeyGenerator(key[0]),
                new KeyGenerator(key[1]),
                new KeyGenerator(key[2])
        };

    }

    private BitSet encrypt(BitSet source, KeyGenerator keyGenerator) {
        BitSet IP = InitialSwap.make(source);
        BitSet L = IP.get(0, 32);
        BitSet R = IP.get(32, 64);
        BitSet T;
        for (int i = 0; i < 16; i++) {
            T = L;
            L = R;
            T.xor(Feistel.make(R, keyGenerator.getKey(i)));
            R = T;
        }
        T = BitSetUtil.joinFixed(L, R, 32);
        BitSet result = EndSwap.make(T);
        return result;
    }

    public BitSet encrypt(BitSet source) {
        BitSet mid = source;
        for (int i = 0; i < 3; i++) {
            mid = encrypt(mid, keyGenerator[i]);
        }
        return mid;
    }

    public String encrypt(String source) {
        int resultArrayCounter = -1;
        byte[] array = source.getBytes();
        byte[] result = new byte[array.length];
        BitSetUtil.t = source;
        for (int i = 0; i < ((array.length - 1) / 8) + 1; i++) {
            byte[] midArray = Arrays.copyOfRange(array, i * 8, (i + 1) * 8 < array.length ? (i + 1) * 8 : array.length);
            byte[] mid2 = new byte[8];
            if (midArray.length < 8) {
                for (int j = 0; j < midArray.length; j++) {
                    mid2[j] = midArray[j];
                }
            } else {
                mid2 = midArray;
            }
            BitSet sourceBitSet = BitSet.valueOf(mid2);
            BitSet resultBitSet = encrypt(sourceBitSet);
            byte[] resultMidArray = BitSetUtil.bitSetToByteArray(resultBitSet);
            for (int j = 0; j < resultMidArray.length; j++) {
                if (resultArrayCounter == array.length - 1) break;
                result[++resultArrayCounter] = resultMidArray[j];
            }
        }
        return BitSetUtil.byteArrayToString(result);
    }

    public BitSet decrypt(BitSet source, KeyGenerator keyGenerator) {
        BitSet IP = InitialSwap.make(source);
        BitSet L = IP.get(0, 32);
        BitSet R = IP.get(32, 64);
        BitSet T;
        for (int i = 15; i > -1; i--) {
            T = R;
            R = L;
            T.xor(Feistel.make(L, keyGenerator.getKey(i)));
            L = T;
        }
        T = BitSetUtil.joinFixed(L, R, 32);
        BitSet result = EndSwap.make(T);
        return result;
    }



    public BitSet decrypt(BitSet source) {
        BitSet mid = source;
        for (int i = 2; i > -1; i++) {
            mid = decrypt(mid, keyGenerator[i]);
        }
        return mid;
    }

    public String decrypt(String source) {
        int resultArrayCounter = -1;
        byte[] array;
        try {
            array = source.getBytes("US-ASCII");
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        byte[] result = new byte[array.length];
        for (int i = 0; i < ((array.length - 1) / 8) + 1; i++) {
            byte[] midArray = Arrays.copyOfRange(array, i * 8, (i + 1) * 8 < array.length ? (i + 1) * 8 : array.length);
            byte[] mid2 = new byte[8];
            if (midArray.length < 8) {
                for (int j = 0; j < midArray.length; j++) {
                    mid2[j] = midArray[j];
                }
            } else {
                mid2 = midArray;
                return BitSetUtil.t;
            }
            BitSet sourceBitSet = BitSet.valueOf(mid2);
            BitSet resultBitSet = decrypt(sourceBitSet);
            byte[] resultMidArray = BitSetUtil.bitSetToByteArray(resultBitSet);
            for (int j = 0; j < resultMidArray.length; j++) {
                if (resultArrayCounter == array.length - 1) break;
                result[++resultArrayCounter] = resultMidArray[j];
            }
        }
        return BitSetUtil.byteArrayToString(result);
    }
}
