package lr2.algorithms.desalghorithms.swaps;

import java.util.BitSet;

public class InitialSwap {
    protected static final int LENGTH = 64;
    protected static final int[] SWAP_POSITIONS = new int[]
            {57, 49, 41, 33, 25, 17,  9,  1, 59, 51, 43, 35, 27, 19, 11,  3,
             61, 53, 45, 37, 29, 21, 13,  5, 63, 55, 47, 39, 31, 23, 15,  7,
             56, 48, 40, 32, 24, 16,  8,  0, 58, 50, 42, 34, 26, 18, 10,  2,
             60, 52, 44, 36, 28, 20, 12,  4, 62, 54, 46, 38, 30, 22, 14,  6};

    public static BitSet make(BitSet source) {
        BitSet result = new BitSet(LENGTH);
        for (int i = 0; i < LENGTH; i++) {
            result.set(i, source.get(SWAP_POSITIONS[i]));
        }
        return result;
    }
}
