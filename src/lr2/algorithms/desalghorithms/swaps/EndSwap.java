package lr2.algorithms.desalghorithms.swaps;

import java.util.BitSet;

public class EndSwap {
    protected static final int LENGTH = 64;
    protected static final int[] SWAP_POSITIONS = new int[]
            {40,  8, 48, 16, 56, 24, 64, 32, 39,  7, 47, 15, 55, 23, 63, 31,
             38,  6, 46, 14, 54, 22, 62, 30, 37,  5, 45, 13, 53, 21, 61, 29,
             36,  4, 44, 12, 52, 20, 60, 28, 35,  3, 43, 11, 51, 19, 59, 27,
             34,  2, 42, 10, 50, 18, 58, 26, 33,  1, 41,  9, 49, 17, 57, 25};

    public static BitSet make(BitSet source) {
        BitSet result = new BitSet(LENGTH);
        for (int i = 0; i < LENGTH; i++) {
            result.set(i, source.get(SWAP_POSITIONS[i] - 1));
        }
        return result;
    }
}
