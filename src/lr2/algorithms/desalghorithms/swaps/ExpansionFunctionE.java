package lr2.algorithms.desalghorithms.swaps;

import java.util.BitSet;

public class ExpansionFunctionE {
    protected static final int LENGTH1 = 32;
    protected static final int LENGTH2 = 48;
    protected static final int[] SWAP_POSITIONS = new int[]
            {31,  0,  1,  2,  3,  4,  3,  4,  5,  6,  7,  8,  7,  8,  9, 10,
             11, 12, 11, 12, 13, 14, 15, 16, 15, 16, 17, 18, 19, 20, 19, 20,
             21, 22, 23, 24, 23, 24, 25, 26, 27, 28, 27, 28, 29, 30, 31,  0};

    public static BitSet make(BitSet source) {
        BitSet result = new BitSet(LENGTH2);
        for (int i = 0; i < LENGTH2; i++) {
            result.set(i, source.get(SWAP_POSITIONS[i]));
        }
        return result;
    }
}
