package lr2.algorithms.desalghorithms.swaps;

import java.util.BitSet;

public class PSwap {
    protected static final int LENGTH = 32;
    protected static final int[] SWAP_POSITIONS = new int[]
            {16,  7, 20, 21, 29, 12, 28, 17,
              1, 15, 23, 26,  5, 18, 31, 10,
              2,  8, 24, 14, 32, 27,  3,  9,
             19, 13, 30,  6, 22, 11,  4, 25};

    public static BitSet make(BitSet source) {
        BitSet result = new BitSet(LENGTH);
        for (int i = 0; i < LENGTH; i++) {
            result.set(i, source.get(SWAP_POSITIONS[i] - 1));
        }
        return result;
    }
}
