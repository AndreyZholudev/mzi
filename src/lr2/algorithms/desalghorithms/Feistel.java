package lr2.algorithms.desalghorithms;

import lr2.algorithms.desalghorithms.swaps.ExpansionFunctionE;
import lr2.algorithms.desalghorithms.swaps.PSwap;
import lr2.algorithms.desalghorithms.swaps.SSwap;

import java.util.BitSet;

public class Feistel {
    public static BitSet make(BitSet R, BitSet ki) {
        BitSet E = ExpansionFunctionE.make(R);
        E.xor(ki);
        BitSet S = SSwap.make(E);
        BitSet P = PSwap.make(S);
        return P;
    }
}
