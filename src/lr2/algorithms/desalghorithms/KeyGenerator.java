package lr2.algorithms.desalghorithms;

import common.BitSetUtil;

import java.util.BitSet;

public class KeyGenerator {
    protected static final int[] SWAP_POSITIONS_C = new int[]
            {57, 49, 41, 33, 25, 17,  9,  1, 58, 50, 42, 34, 26, 18,
             10,  2, 59, 51, 43, 35, 27, 19, 11,  3, 60, 52, 44, 36};
    protected static final int[] SWAP_POSITIONS_D = new int[]
            {63, 55, 47, 39, 31, 23, 15,  7, 62, 54, 46, 38, 30, 22,
             14,  6, 61, 53, 45, 37, 29, 21, 13,  5, 28, 20, 12,  4};

    protected static final int[] SWAP_POSITIONS_Ki = new int[]
            {14, 17, 11, 24,  1,  5,  3, 28, 15,  6, 21, 10, 23, 19, 12,  4,
             26,  8, 16,  7, 27, 20, 13,  2, 41, 52, 31, 37, 47, 55, 30, 40,
             51, 45, 33, 48, 44, 49, 39, 56, 34, 53, 46, 42, 50, 36, 29, 32};
    protected static final byte[] MOVE_LEFT_NUM = new byte[]
            {1, 1, 2, 2, 2, 2, 2, 2, 1, 2, 2, 2, 2, 2, 2, 1};
    private BitSet key;
    private BitSet C, D;
    private BitSet[] keys = new BitSet[16];


    public KeyGenerator(BitSet key) {
        this.key = key;
        initiateBlocks();
        generateKeys();
    }

    private void initiateBlocks() {
        BitSet t = new BitSet(64);
        int counter;
        for (int i = 7; i >= 0; i--) {
            counter = 0;
            for (int j = 7 * i; j < 7 * (i + 1); j++) {
                if (key.get(j)) {
                    counter++;
                    t.set(i * 8 + (j - 7 * i));
                }
            }
            if (counter % 2 == 0) {
                t.set(8 * (i + 1) - 1);
            }
        }
        C = new BitSet(28);
        D = new BitSet(28);
        for (int i = 0; i < 28; i++) {
            C.set(i, t.get(SWAP_POSITIONS_C[i]));
            D.set(i, t.get(SWAP_POSITIONS_D[i]));
        }
    }

    private void generateKeys() {
        for (int i = 0; i < 16; i++) {
            C = moveLeft(C, MOVE_LEFT_NUM[i]);
            D = moveLeft(D, MOVE_LEFT_NUM[i]);
            BitSet t = BitSetUtil.joinFixed(C, D, 28);
            BitSet ki = new BitSet(48);
            for (int j = 0; j < 48; j++) {
                ki.set(j, t.get(SWAP_POSITIONS_Ki[j]));
            }
            keys[i] = ki;
        }
    }

    private BitSet moveLeft(BitSet source, byte num) {
        BitSet result = new BitSet(28);
        for (int i = num; i < 28; i++) {
            result.set(i - num, source.get(i));
        }
        for (int i = 28 - num; i < 28; i++) {
            result.set(i, source.get(i - (28 - num)));
        }
        return result;
    }

    public BitSet getKey(int num) {
        return keys[num];
    }
}
