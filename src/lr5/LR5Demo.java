package lr5;

import common.Demo;
import lr5.algorithm.PearsonHash;

public class LR5Demo implements Demo {
    protected final String HASHING_MESSAGE = "Hashing demonstration:";
    protected final String TEST_TEXT_1 = "ABCDEFGHIJKLMNOPQRSTUVWXYZ abcdefghijklmnopqrstuvwxyz";
    protected final String TEST_TEXT_1_2 = "BBCDEFGHIJKLMNOPQRSTUVWXYZ abcdefghijklmnopqrstuvwxyz";
    protected final String TEST_TEXT_2 = "As a fragment of soul, a Horcrux seemed to retain the identity of its " +
            "creator at the time of its creation. Voldemort, for instance, created a Horcrux during what was " +
            "presumably his fifth year at Hogwarts. As such, the fragment of soul contained within the Horcrux " +
            "took on the appearance and mannerisms of Voldemort as he had been when he was 16 years old.";
    protected final String TEST_TEXT_2_2 = "As a fragment of soul, a Horcrux seemed to retain the identity of its " +
            "creator at the time of its creation. Woldemort, for instance, created a Horcrux during what was " +
            "presumably his fifth year at Hogwarts. As such, the fragment of soul contained within the Horcrux " +
            "took on the appearance and mannerisms of Voldemort as he had been when he was 16 years old.";
    byte[] bytes = new byte[] {12, 65, 34, 65, 84};

    public void run() {
        PearsonHash hash = new PearsonHash();
        System.out.println(HASHING_MESSAGE);
        System.out.println(TEST_TEXT_1);
        System.out.println(hash.makeHash(TEST_TEXT_1));
        System.out.println(TEST_TEXT_1_2);
        System.out.println(hash.makeHash(TEST_TEXT_1_2));

        System.out.println("\n" + TEST_TEXT_2);
        System.out.println(hash.makeHash(TEST_TEXT_2));
        System.out.println(TEST_TEXT_2_2);
        System.out.println(hash.makeHash(TEST_TEXT_2_2));
    }
}
