package lr5.algorithm;

import common.Util;

public class PearsonHash {
    private final static long p = 4246987324863246353l;
    private static final int BYTE_LENGTH = 8;

    private long hash = 0;

    public long makeHash(String source) {
        hash = 0;
        splitSourceString(source);
        return hash;
    }

    public long makeHash(byte[] source) {
        if (source.length != 8) {
            throw new RuntimeException("Length of byte array should be 8!");
        }
        hash = 0;
        updateHash(source);
        return hash;
    }

    private void splitSourceString(String data)  {
        if (data.length() % BYTE_LENGTH > 0) {
            int rest = BYTE_LENGTH - (data.length() % BYTE_LENGTH);
            for(int i = 0; i < rest; i++) {
                data += " ";
            }
        }
        int nParts = data.length() / BYTE_LENGTH;
        String partStr;
        byte[] partByte;
        for(int p = 0; p < nParts; p++) {
            partStr = data.substring(p * BYTE_LENGTH, p * BYTE_LENGTH + BYTE_LENGTH);
            partByte = Util.stringToByteArray(partStr);
            updateHash(partByte);
        }
    }

    private void updateHash(byte[] next) {
        long value = 0;
        for (int i = 0; i < next.length; i++)
        {
            value += (value ^ (((long) next[i] & 0xffL) << (8 * i))) * p;
        }

        hash = hash ^ value;
    }
}
