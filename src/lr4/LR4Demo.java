package lr4;

import common.Demo;
import lr4.algorithm.RSA;

public class LR4Demo implements Demo {
    protected final String ENCODING_MESSAGE = "Encoding/decoding demonstration:";
    protected final String TEST_TEXT_1 = "ABCDEFGHIJKLMNOPQRSTUVWXYZ abcdefghijklmnopqrstuvwxyz";
    protected final String TEST_TEXT_2 = "As a fragment of soul, a Horcrux seemed to retain the identity of its " +
            "creator at the time of its creation. Voldemort, for instance, created a Horcrux during what was " +
            "presumably his fifth year at Hogwarts. As such, the fragment of soul contained within the Horcrux " +
            "took on the appearance and mannerisms of Voldemort as he had been when he was 16 years old.";

    public void run() {
        RSA rsa = new RSA();
        String encryptedData = null;
        String decryptedData = null;
        try {
            encryptedData = rsa.encrypt(TEST_TEXT_1);
            decryptedData = rsa.decrypt(encryptedData);
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println(ENCODING_MESSAGE);
        System.out.println(TEST_TEXT_1);
        System.out.println(encryptedData);
        System.out.println(decryptedData);

        System.out.println("\n");
        encryptedData = rsa.encrypt(TEST_TEXT_2);
        decryptedData = rsa.decrypt(encryptedData);
        System.out.println("\n\n" + TEST_TEXT_2);
        System.out.println(new String(encryptedData));
        System.out.println(new String(decryptedData));
    }
}
