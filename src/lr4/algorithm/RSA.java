package lr4.algorithm;

import common.Util;
import lr3.algorithm.utils.Mode;

import java.math.BigInteger;
import java.util.Random;

public class RSA {
    private static int BIT_LENGTH = 1024;
    private static int BYTE_LENGTH = 256;
    private BigInteger p, q, N, phi, e, d;
    private Random r = new Random();

    public RSA()
    {
        p = BigInteger.probablePrime(BIT_LENGTH, r);
        q = BigInteger.probablePrime(BIT_LENGTH, r);
        N = p.multiply(q);
        phi = p.subtract(BigInteger.ONE).multiply(q.subtract(BigInteger.ONE));
        e = BigInteger.probablePrime(BIT_LENGTH / 2, r);
        while (phi.gcd(e).compareTo(BigInteger.ONE) > 0 && e.compareTo(phi) < 0)
        {
            e.add(BigInteger.ONE);
        }
        d = e.modInverse(phi);
    }

    public RSA(int BIT_LENGTH,int BYTE_LENGTH) {
        RSA.BIT_LENGTH = BIT_LENGTH;
        RSA.BYTE_LENGTH = BYTE_LENGTH;
    }

    public RSA(BigInteger e, BigInteger d, BigInteger N)
    {
        this.e = e;
        this.d = d;
        this.N = N;
    }

    public String encrypt(String data) {
        return splitSourceString(data, Mode.ENCRYPTION);
    }
    public String decrypt(String data) {
        return splitSourceString(data, Mode.DECRYPTION);
    }

    private String splitSourceString(String data, Mode mode)  {
        if (data.length() % BYTE_LENGTH > 0) {
            int rest = BYTE_LENGTH - (data.length() % BYTE_LENGTH);
            for(int i = 0; i < rest; i++) {
                data += " ";
            }
        }
        int nParts = data.length() / BYTE_LENGTH;
        byte[] res = new byte[data.length()];
        String partStr;
        byte[] partByte;
        for(int p = 0; p < nParts; p++) {
            partStr = data.substring(p * BYTE_LENGTH, (p + 1) * BYTE_LENGTH);
            partByte = Util.stringToByteArray(partStr);
            partByte = (mode == Mode.ENCRYPTION) ?
                    encrypt(partByte) :
                    decrypt(partByte);
            for(int b = 0; b < BYTE_LENGTH; b++) {
                res[p * BYTE_LENGTH + b] = partByte[b];
            }
        }

        return Util.byteArrayToString(res);
    }

    public byte[] encrypt(byte[] message) {
        return makeProcess(e, message);
    }

    public byte[] decrypt(byte[] message) {
        return makeProcess(d, message);
    }

    private byte[] makeProcess(BigInteger key, byte[] message) {
        return (new BigInteger(message)).modPow(key, N).toByteArray();
    }

    public void setN(BigInteger n) {
        N = n;
    }

    public void setE(BigInteger e) {
        this.e = e;
    }

    public void setD(BigInteger d) {
        this.d = d;
    }
}
