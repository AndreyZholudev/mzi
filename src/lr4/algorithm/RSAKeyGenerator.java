package lr4.algorithm;

import common.Util;
import javafx.util.Pair;

import java.math.BigInteger;
import java.util.Random;

public class RSAKeyGenerator {
    private final static int BIT_LENGTH = Constants.BIT_LENGTH;
    private final static int BYTE_LENGTH = Constants.BYTE_LENGTH;
    private BigInteger p, q, N, phi, e, d;
    private Random r = new Random();

    public RSAKeyGenerator() {
        p = BigInteger.probablePrime(BIT_LENGTH, r);
        q = BigInteger.probablePrime(BIT_LENGTH, r);
        N = p.multiply(q);
        phi = p.subtract(BigInteger.ONE).multiply(q.subtract(BigInteger.ONE));
        e = BigInteger.probablePrime(BIT_LENGTH / 2, r);
        while (phi.gcd(e).compareTo(BigInteger.ONE) > 0 && e.compareTo(phi) < 0)
        {
            e.add(BigInteger.ONE);
        }
        d = e.modInverse(phi);
    }

    public Pair<BigInteger, BigInteger> getOpenKey() {
        return new Pair<>(e, N);
    }

    public Pair<BigInteger, BigInteger> getCloseKey() {
        return new Pair<>(d, N);
    }

    public static Pair<BigInteger, BigInteger> getPredefinedOpenKey() {
        return new Pair<>(
                new BigInteger(Util.longToByteArray(47533l)),
                new BigInteger(Util.longToByteArray(6515520873068689099l))
        );
    }

    public static Pair<BigInteger, BigInteger> getPredefinedCloseKey() {
        return new Pair<>(
                new BigInteger(Util.longToByteArray(1200765001226302117l)),
                new BigInteger(Util.longToByteArray(6515520873068689099l))
        );
    }
}
