package lr4.algorithm;

public abstract class Constants {
    public static final int BIT_LENGTH = 32;
    public static final int BYTE_LENGTH = 8;
}
