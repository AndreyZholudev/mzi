package lr6;

import common.Demo;
import javafx.util.Pair;
import lr4.algorithm.RSAKeyGenerator;
import lr6.algorithm.DS;

import java.math.BigInteger;

public class LR6Demo implements Demo {
    protected final String TEST_TEXT_1 = "ABCDEFGHIJKLMNOPQRSTUVWXYZ abcdefghijklmnopqrstuvwxyz";
    protected final String TEST_TEXT_1_2 = "ABCDEFGHIJKLMNOPQRSTUVWXYZ abcdefghijklmnopqrstuvwxyz";
    protected final String TEST_TEXT_2 = "As a fragment of soul, a Horcrux seemed to retain the identity of its " +
            "creator at the time of its creation. Voldemort, for instance, created a Horcrux during what was " +
            "presumably his fifth year at Hogwarts. As such, the fragment of soul contained within the Horcrux " +
            "took on the appearance and mannerisms of Voldemort as he had been when he was 16 years old.";
    protected final String TEST_TEXT_2_2 = "As a fragment of soul, a Horcrux seemed to retain the identity of its " +
            "creator at the time of its creation. Voldemort, for instance, created a Horcrux during what was " +
            "presumably his fifth year at Hogwarts. As such, the fragment of soul contained within the Horcrux " +
            "took on the appearance and mannerisms of Voldemort as he had been when he was 16 years old.";

    public void run() {
        DS cp = new DS();
        Pair<BigInteger, BigInteger> closeKey = RSAKeyGenerator.getPredefinedCloseKey();
        Pair<BigInteger, BigInteger> openKey = RSAKeyGenerator.getPredefinedOpenKey();
        System.out.println("Signature:");
        Long signature = cp.sign(TEST_TEXT_1, closeKey);
        System.out.println(signature);

        System.out.println("\nCheck signature with unchanged message:");
        System.out.println(cp.checkMessage(TEST_TEXT_1, signature, openKey));

        System.out.println("\nCheck signature with slightly changed message:");
        System.out.println(cp.checkMessage(TEST_TEXT_1_2, signature, openKey));
    }
}
