package lr6.algorithm;

public class Message {
    private String message;
    private String mKey;

    public Message(String message, String mKey) {
        this.message = message;
        this.mKey = mKey;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getmKey() {
        return mKey;
    }

    public void setmKey(String mKey) {
        this.mKey = mKey;
    }
}
