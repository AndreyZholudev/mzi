package lr6.algorithm;

import common.Util;
import javafx.util.Pair;
import lr4.algorithm.RSA;
import lr5.algorithm.PearsonHash;

import java.math.BigInteger;

public class DS {
    private PearsonHash pearsonHash = new PearsonHash();

    public Long sign(String message, Pair<BigInteger, BigInteger> closeKey) {
        RSA rsa = new RSA(32, 8);
        rsa.setD(closeKey.getKey());
        rsa.setN(closeKey.getValue());

        Long hash = pearsonHash.makeHash(message);
        byte[] array = rsa.decrypt(Util.longToByteArray(hash));

        return Util.byteArrayToLong(array);
    }

    public boolean checkMessage(String message, Long signature, Pair<BigInteger, BigInteger> openKey) {
        RSA rsa = new RSA(32, 8);
        rsa.setE(openKey.getKey());
        rsa.setN(openKey.getValue());

        Long hash = pearsonHash.makeHash(message);
        byte[] array = rsa.encrypt(Util.longToByteArray(signature));
        Long receivedHash = Util.byteArrayToLong(array);

        return hash.equals(receivedHash);
    }
}
