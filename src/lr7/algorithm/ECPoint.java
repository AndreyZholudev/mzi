package lr7.algorithm;

import java.math.BigInteger;

public class ECPoint {

    public final static BigInteger TWO = new BigInteger("2");
    public final static BigInteger THREE = new BigInteger("3");

    private EllipticCurve curve;

    private BigInteger x, y;
    private boolean isZero;

    private ECPoint[] fastcache = null;
    private ECPoint[] cache = null;

    public void fastCache() {
        if (fastcache == null) {
            fastcache = new ECPoint[256];
            fastcache[0] = new ECPoint(curve);
            for (int i = 1; i < fastcache.length; i++) {
                fastcache[i] = fastcache[i - 1].add(this);
            }
        }
    }

    public ECPoint(EllipticCurve curve, BigInteger x, BigInteger y) {
        this.curve = curve;
        this.x = x;
        this.y = y;
        isZero = false;
    }

    public ECPoint(byte[] bytes, EllipticCurve curve) {
        this.curve = curve;
        if (bytes[0] == 2) {
            isZero = true;
            return;
        }
        boolean ymt = false;
        if (bytes[0] != 0) ymt = true;
        bytes[0] = 0;
        x = new BigInteger(bytes);
        y = x.multiply(x).add(curve.geta()).multiply(x).add(curve.getb()).modPow(curve.getPPODBF(), curve.getp());
        if (ymt != y.testBit(0)) {
            y = curve.getp().subtract(y);
        }
        isZero = false;
    }

    public ECPoint(EllipticCurve e) {
        x = y = BigInteger.ZERO;
        curve = e;
        isZero = true;
    }

    public byte[] compress() {
        byte[] cmp = new byte[curve.getPCS()];
        if (isZero) {
            cmp[0] = 2;
        }
        byte[] xb = x.toByteArray();
        System.arraycopy(xb, 0, cmp, curve.getPCS() - xb.length, xb.length);
        if (y.testBit(0)) cmp[0] = 1;
        return cmp;
    }

    public ECPoint add(ECPoint q) {

        if (this.isZero) return q;
        else if (q.isZero()) return this;

        BigInteger y1 = y;
        BigInteger y2 = q.gety();
        BigInteger x1 = x;
        BigInteger x2 = q.getx();

        BigInteger alpha;

        if (x2.compareTo(x1) == 0) {

            if (!(y2.compareTo(y1) == 0)) return new ECPoint(curve);
            else {
                alpha = ((x1.modPow(TWO, curve.getp())).multiply(THREE)).add(curve.geta());
                alpha = (alpha.multiply((TWO.multiply(y1)).modInverse(curve.getp()))).mod(curve.getp());
            }

        } else {
            alpha = ((y2.subtract(y1)).multiply((x2.subtract(x1)).modInverse(curve.getp()))).mod(curve.getp());
        }

        BigInteger x3, y3;
        x3 = (((alpha.modPow(TWO, curve.getp())).subtract(x2)).subtract(x1)).mod(curve.getp());
        y3 = ((alpha.multiply(x1.subtract(x3))).subtract(y1)).mod(curve.getp());


        return new ECPoint(curve, x3, y3);

    }

    public ECPoint multiply(BigInteger coef) {
        ECPoint result = new ECPoint(curve);
        byte[] coefb = coef.toByteArray();
        if (fastcache != null) {
            for (int i = 0; i < coefb.length; i++) {
                result = result.times256().add(fastcache[coefb[i] & 255]);
            }
            return result;
        }
        if (cache == null) {
            cache = new ECPoint[16];
            cache[0] = new ECPoint(curve);
            for (int i = 1; i < cache.length; i++) {
                cache[i] = cache[i - 1].add(this);
            }
        }
        for (int i = 0; i < coefb.length; i++) {
            result = result.times16().add(cache[(coefb[i] >> 4) & 15]).times16().add(cache[coefb[i] & 15]);
        }
        return result;
    }

    private ECPoint times16() {
            ECPoint result = this;
            for (int i = 0; i < 4; i++) {
                result = result.add(result);
            }
            return result;
    }

    private ECPoint times256() {
            ECPoint result = this;
            for (int i = 0; i < 8; i++) {
                result = result.add(result);
            }
            return result;
    }

    public BigInteger getx() {
        return x;
    }

    public BigInteger gety() {
        return y;
    }

    public String toString() {
        return "(" + x.toString() + ", " + y.toString() + ")";
    }

    public boolean isZero() {
        return isZero;
    }
}
