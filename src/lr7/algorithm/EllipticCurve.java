package lr7.algorithm;

/**
 * An implementation of an elliptic curve over a -finite- field.
 */

import java.math.BigInteger;

public class EllipticCurve {
    public static final BigInteger COEFA = new BigInteger("4");
    public static final BigInteger COEFB = new BigInteger("27");

    private BigInteger a, b, p, order;
    private ECPoint generator;
    private BigInteger ppodbf;
    private int pointCmpSize;
    private String name;

    public EllipticCurve(BigInteger a, BigInteger b, BigInteger p) {

        this.a = a;
        this.b = b;
        this.p = p;

        byte[] pb = p.toByteArray();
        if (pb[0] == 0) pointCmpSize = pb.length;
        else pointCmpSize = pb.length + 1;
        name = "";
    }

    public EllipticCurve(ECParameters ecp) {
        this(ecp.a(), ecp.b(), ecp.p());
        order = ecp.order();
        name = ecp.toString();
        generator = new ECPoint(this, ecp.generatorX(), ecp.generatorY());
        generator.fastCache();
    }

    public boolean isSingular() {

        BigInteger aa = a.pow(3);
        BigInteger bb = b.pow(2);

        BigInteger result = ((aa.multiply(COEFA)).add(bb.multiply(COEFB))).mod(p);

        if (result.compareTo(BigInteger.ZERO) == 0) return true;
        else return false;
    }

    public boolean onCurve(ECPoint q) {
        if (q.isZero()) return true;
        BigInteger y_square = (q.gety()).modPow(new BigInteger("2"), p);
        BigInteger x_cube = (q.getx()).modPow(new BigInteger("3"), p);
        BigInteger x = q.getx();

        BigInteger dum = ((x_cube.add(a.multiply(x))).add(b)).mod(p);

        if (y_square.compareTo(dum) == 0) return true;
        else return false;
    }

    public BigInteger getOrder() {
        return order;
    }

    public BigInteger geta() {
        return a;
    }

    public BigInteger getb() {
        return b;
    }

    public BigInteger getp() {
        return p;
    }

    public int getPCS() {
        return pointCmpSize;
    }

    public ECPoint getGenerator() {
        return generator;
    }

    public String toString() {
        if (name == null) return "y^2 = x^3 + " + a + "x + " + b + " ( mod " + p + " )";
        else if (name.equals("")) return "y^2 = x^3 + " + a + "x + " + b + " ( mod " + p + " )";
        else return name;
    }

    public BigInteger getPPODBF() {
        if (ppodbf == null) {
            ppodbf = p.add(BigInteger.ONE).shiftRight(2);
        }
        return ppodbf;
    }
}

