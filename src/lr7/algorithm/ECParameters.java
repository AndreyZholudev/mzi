package lr7.algorithm;

import java.math.BigInteger;

interface ECParameters {

    BigInteger a();

    BigInteger b();

    BigInteger p();

    BigInteger generatorX();

    BigInteger generatorY();

    BigInteger order();

    String toString();
}
