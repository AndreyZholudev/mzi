package lr7.algorithm;

import java.math.BigInteger;

public class P256 implements ECParameters {
    private static final BigInteger p
            = new BigInteger("ffffffff00000001000000000000000000000000ffffffffffffffffffffffff", 16);
    private static final BigInteger a
            = new BigInteger("ffffffff00000001000000000000000000000000fffffffffffffffffffffffc", 16);
    private static final BigInteger b
            = new BigInteger("5ac635d8aa3a93e7b3ebbd55769886bc651d06b0cc53b0f63bce3c3e27d2604b", 16);
    private static final BigInteger gx
            = new BigInteger("6b17d1f2e12c4247f8bce6e563a440f277037d812deb33a0f4a13945d898c296", 16);
    private static final BigInteger gy
            = new BigInteger("4fe342e2fe1a7f9b8ee7eb4a7c0f9e162bce33576b315ececbb6406837bf51f5", 16);
    private static final BigInteger n
            = new BigInteger("ffffffff00000000ffffffffffffffffbce6faada7179e84f3b9cac2fc632551", 16);

    public BigInteger a() {
        return a;
    }

    public BigInteger b() {
        return b;
    }

    public BigInteger p() {
        return p;
    }

    public BigInteger generatorX() {
        return gx;
    }

    public BigInteger generatorY() {
        return gy;
    }

    public BigInteger order() {
        return n;
    }

    public String toString() {
        return "secp256r1";
    }
}
