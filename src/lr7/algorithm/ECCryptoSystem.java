package lr7.algorithm;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.SecureRandom;

public class ECCryptoSystem {
    private final int HASH_SIZE = 20;

    private MessageDigest hash;
    private SecureRandom random = new SecureRandom();
    private EllipticCurve ec;
    private ECKey key;

    public ECCryptoSystem(EllipticCurve ec) {
        this.ec = ec;
        this.key = new ECKey(ec);
        try {
            hash = MessageDigest.getInstance("SHA-1");
        } catch (java.security.NoSuchAlgorithmException e) {
            System.out.println("RSACryptoSystem: THIS CANNOT HAPPEN\n" + e);
            System.exit(0);
        }
    }

    public byte[] encrypt(byte[] input) {
        ECKey ek = key;
        int numbytes = input.length;
        byte[] res = new byte[ek.curve.getPCS() + numbytes];
        hash.reset();

        BigInteger rk = new BigInteger(ek.curve.getp().bitLength() + 17, random);
        if (ek.curve.getOrder() != null) {
            rk = rk.mod(ek.curve.getOrder());
        }
        ECPoint gamma = ek.curve.getGenerator().multiply(rk);
        ECPoint sec = ek.beta.multiply(rk);
        System.arraycopy(gamma.compress(), 0, res, 0, ek.curve.getPCS());
        hash.update(sec.getx().toByteArray());
        hash.update(sec.gety().toByteArray());
        byte[] digest = hash.digest();
        for (int j = 0; j < numbytes; j++) {
            res[j + ek.curve.getPCS()] = (byte) (input[j] ^ digest[j % HASH_SIZE]);
        }
        return res;
    }

    public byte[] decrypt(byte[] input) {
        ECKey dk = key;
        byte[] res = new byte[input.length - dk.curve.getPCS()];
        byte[] gammacom = new byte[dk.curve.getPCS()];
        hash.reset();

        System.arraycopy(input, 0, gammacom, 0, dk.curve.getPCS());
        ECPoint gamma = new ECPoint(gammacom, dk.curve);
        ECPoint sec = gamma.multiply(dk.sk);
        if (sec.isZero()) {
            hash.update(BigInteger.ZERO.toByteArray());
            hash.update(BigInteger.ZERO.toByteArray());
        } else {
            hash.update(sec.getx().toByteArray());
            hash.update(sec.gety().toByteArray());
        }
        byte[] digest = hash.digest();
        for (int j = 0; j < input.length - dk.curve.getPCS(); j++) {
            res[j] = (byte) (input[j + dk.curve.getPCS()] ^ digest[j % HASH_SIZE]);
        }
        return res;
    }

    public void setKey(ECKey key) {
        this.key = key;
    }

    public String toString() {
        return "ECC - " + ec.toString();
    }
}
