package lr7.algorithm;

import java.math.BigInteger;
import java.security.SecureRandom;

public class ECKey {
    private final static int OFFSET = 17;

    protected boolean secret;
    protected BigInteger sk;
    protected ECPoint beta;
    protected EllipticCurve curve;
    protected SecureRandom random = new SecureRandom();

    public ECKey(EllipticCurve ec) {
        curve = ec;
        secret = true;
        sk = new BigInteger(ec.getp().bitLength() + OFFSET, random);
        if (curve.getOrder() != null) sk = sk.mod(curve.getOrder());
        beta = (curve.getGenerator()).multiply(sk);
        beta.fastCache();
    }

    public String toString() {
        if (secret) return ("Secret key: " + sk + " " + beta + " " + curve);
        else return ("Public key:" + beta + " " + curve);
    }

    public boolean isPublic() {
        return (!secret);
    }

    public ECKey getPublic() {
        ECKey temp = new ECKey(curve);
        temp.beta = beta;
        temp.sk = BigInteger.ZERO;
        temp.secret = false;
        System.gc();
        return temp;
    }
}
