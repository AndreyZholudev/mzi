package lr7;

import common.Demo;
import common.Util;
import lr7.algorithm.ECCryptoSystem;
import lr7.algorithm.EllipticCurve;
import lr7.algorithm.P256;

public class LR7Demo implements Demo {
    protected final String ENCODING_MESSAGE = "Encoding/decoding demonstration:";
    protected final String TEST_TEXT_1 = "ABCDEFGHIJKLMNOPQRSTUVWXYZ abcdefghijklmnopqrstuvwxyz";
    protected final String TEST_TEXT_2 = "As a fragment of soul, a Horcrux seemed to retain the identity of its " +
            "creator at the time of its creation. Voldemort, for instance, created a Horcrux during what was " +
            "presumably his fifth year at Hogwarts. As such, the fragment of soul contained within the Horcrux " +
            "took on the appearance and mannerisms of Voldemort as he had been when he was 16 years old.";

    public void run() {
        String encryptedData;
        String decryptedData;

        EllipticCurve ellipticCurve = new EllipticCurve(new P256());
        ECCryptoSystem cryptoSystem = new ECCryptoSystem(ellipticCurve);

        System.out.println(ENCODING_MESSAGE);
        System.out.println(TEST_TEXT_1);
        System.out.println("\n");
        encryptedData = Util.byteArrayToString(cryptoSystem.encrypt(Util.stringToByteArray(TEST_TEXT_1)));
        decryptedData = Util.byteArrayToString(cryptoSystem.decrypt(Util.stringToByteArray(encryptedData)));
        System.out.println(new String(encryptedData));
        System.out.println(new String(decryptedData));

        System.out.println("\n");
        encryptedData = Util.byteArrayToString(cryptoSystem.encrypt(Util.stringToByteArray(TEST_TEXT_2)));
        decryptedData = Util.byteArrayToString(cryptoSystem.decrypt(Util.stringToByteArray(encryptedData)));
        System.out.println("\n\n" + TEST_TEXT_2);
        System.out.println(new String(encryptedData));
        System.out.println(new String(decryptedData));
    }
}
