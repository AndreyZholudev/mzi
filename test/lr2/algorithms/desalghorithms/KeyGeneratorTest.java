package lr2.algorithms.desalghorithms;

import lr2.algorithms.desalghorithms.swaps.InitialSwap;
import org.junit.Assert;
import org.junit.Test;

import java.util.BitSet;

public class KeyGeneratorTest extends Assert {
    @Test
    public void testMake() {
        BitSet source = BitSet.valueOf(new byte[] {(byte)0b10110100, (byte)0b10101010, 0b00101100,
                0b01011100, 0b01000100, (byte)0b10101100, 0b01110100, (byte)0b11011100});
        BitSet expectedResult = BitSet.valueOf(new byte[] {0b01000000, 0b01110101, (byte)0b11100110,
                (byte)0b11000101, 0b00000000, (byte)0b10111111, (byte)0b10010011, 0b00011011});
        BitSet result = InitialSwap.make(source);
        assertEquals(expectedResult, result);
    }
}
