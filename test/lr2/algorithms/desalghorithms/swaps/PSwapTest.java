package lr2.algorithms.desalghorithms.swaps;

import org.junit.Assert;
import org.junit.Test;

import java.util.BitSet;

public class PSwapTest extends Assert {
    @Test
    public void testMake() {
        BitSet source = BitSet.valueOf(new byte[] {(byte)0b10110100, (byte)0b10101010, 0b00101100,
                0b01011100});
        BitSet expectedResult = BitSet.valueOf(new byte[] {0b01110101, (byte)0b11010000, 0b01101010,
                (byte)0b00011001});
        BitSet result = PSwap.make(source);
        assertEquals(expectedResult, result);
    }
}
