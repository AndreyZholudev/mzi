package lr2.algorithms.desalghorithms.swaps;

import org.junit.Assert;
import org.junit.Test;

import java.util.BitSet;

public class SSwapTest extends Assert {
    @Test
    public void testMake() {
        BitSet source = BitSet.valueOf(new byte[] {0b00000000, 0b00000000, 0b00000000,
                0b00000000, 0b00000000, 0b00000000});
        BitSet expectedResult = BitSet.valueOf(new byte[] {(byte)0b11111110, (byte)0b01111010, (byte)0b11000010,
                (byte)0b11010100});
        BitSet result = SSwap.make(source);
        assertEquals(expectedResult, result);
    }
}
