package lr2.algorithms.desalghorithms.swaps;

import org.junit.Assert;
import org.junit.Test;

import java.util.BitSet;

public class EndSwapTest extends Assert {
    @Test
    public void testMake() {
        BitSet source = BitSet.valueOf(new byte[] {(byte)0b10110100, (byte)0b10101010, 0b00101100,
                0b01011100, 0b01000100, (byte)0b10101100, 0b01110100, (byte)0b11011100});
        BitSet expectedResult = BitSet.valueOf(new byte[] {0b01001110, (byte)0b11010001, 0b00111110,
                (byte)0b11010010, (byte)0b11101100, (byte)0b11110111, 0b00001000, 0b00000000});
        BitSet result = EndSwap.make(source);
        assertEquals(expectedResult, result);
    }
}
