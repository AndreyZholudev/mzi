package lr2.algorithms.desalghorithms.swaps;

import org.junit.Assert;
import org.junit.Test;

import java.util.BitSet;

public class ExpansionFunctionETest extends Assert {
    @Test
    public void testMake() {
        BitSet source = BitSet.valueOf(new byte[] {(byte)0b10110100, (byte)0b10101010, 0b00101100,
                0b01011100, 0b01000100, (byte)0b10101100});
        BitSet expectedResult = BitSet.valueOf(new byte[] {(byte)0b10101000, 0b01010101, 0b01010101,
                0b01011001, (byte)0b10000001, 0b00101111});
        BitSet result = ExpansionFunctionE.make(source);
        assertEquals(expectedResult, result);
    }
}
