package lr3.algorithm;

import org.junit.Assert;
import org.junit.Test;

public class AESTest extends Assert {

    public AESTest() {
    }

    @Test
    public void testEncryptionDecryptionWorks() throws Exception {
        String cryptKey = "aPb4x9q0H4W8rPs7";
        String data = "hello world     fsdfdass                  ";
        AES aes = new AES();
        aes.setKey(cryptKey);
        String encryptedData = aes.encrypt(data);
        String decryptedData = aes.decrypt(encryptedData);
        assertEquals(data.trim(), decryptedData.trim());
    }

}